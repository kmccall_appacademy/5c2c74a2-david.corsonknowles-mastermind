class Code
  attr_accessor :pegs

  PEGS = {
    :r => "Red",
    :g => "Green",
    :b => "Blue",
    :y => "Yellow",
    :o => "Orange",
    :p => "Purple"
  }

  def initialize(array_of_pegs)
    @pegs = array_of_pegs

  end

  def self.parse(string)
    check = string.downcase

    raise "Invalid color" if check.chars.any? { |e| PEGS[e.to_sym].nil? }

    result = self.new(check.chars)
  end

  def self.random
    color_array = PEGS.keys.map(&:to_s)
    random_array = Array.new(4) { color_array.sample }
    self.new(random_array)
  end

  def [](n)
    @pegs[n]
  end

  def ==(code)
    return false unless code.class == Code
    self.pegs == code.pegs
  end

  def exact_matches(other_code)
    count = 0
    self.pegs.each_with_index do |e, i|
      count += 1 if e == other_code.pegs[i]
    end
    count
  end

  def peg_count(code)
    hash_counter = Hash.new(0)
    code.pegs.each do |peg|
      hash_counter[peg] += 1
    end
    hash_counter
  end

  def total_matches(other_code)
    first = peg_count(self)
    second = peg_count(other_code)
    count = 0
    first.each do |k, v|
      if second[k]
        first[k] < second[k] ? lesser = first[k] : lesser = second[k]
        count += lesser
      end
    end
    count
  end

  def near_matches(other_code)
    self.total_matches(other_code) - self.exact_matches(other_code)
  end

end

class Game
  attr_accessor :secret_code

  def initialize(code = nil)
    code ||= Code.random
    @secret_code = code
  end

  def get_guess
    puts "What code will you enter now?\n"
    Code.parse($stdin.gets.chomp)
  end

  # def valid?(guess)
  #   return false unless guess.length == 4
  #   return false unless guess.chars.all? {|e| %w(r g b y o p).include?(e)}
  #   true
  # end

  def display_matches(code)
    puts "exact matches:\n#{secret_code.exact_matches(code)}"
    puts "near matches:\n#{secret_code.near_matches(code)}\n"
  end

  def play
    puts "These are the colors you can choose from: "
    puts " r g b y o p "
    puts "Enter an answer like:  rgby  "
    turn = 1
    until turn > 10
      puts "We are on turn #{turn}\n"

      guess = get_guess
      display_matches(guess)

      puts "You won in #{turn} turns" if secret_code.exact_matches(guess) == 4
      break if secret_code.exact_matches(guess) == 4

      turn += 1
    end
    puts "\nGood game!! Come back and play more any time, partner."
  end

end

if __FILE__ == $PROGRAM_NAME
  game = Game.new
  game.play
end
